package com.example.myWallet.activities

import android.widget.{Button, TextView}
import com.example.myWallet.bo.{Wallet, BaseEntry}
import com.example.myWallet.{DBHelper, R}
import android.app.Activity
import android.os.Bundle
import com.example.myWallet.listeners._
import android.view.View.OnClickListener
import com.example.myWallet.storages.WalletStorage
import android.view.{MenuItem, View, ContextMenu}
import android.view.ContextMenu.ContextMenuInfo
import com.example.myWallet.context_menu_handlers.{MyActivityContextMenuHandler, ContextMenuHandler}

/**
 * User: vkrasnov
 * Date: 13.04.14
 * Time: 14:32
 */

case class MoneyField(valueField: TextView, nameField: TextView)

class MyActivity extends Activity
    with ChangeEntryTypeButtonDefaultStyle
{
    var newEntry = new BaseEntry
    lazy val dbHelper = new DBHelper(this)
    lazy val contextMenuHandler = new MyActivityContextMenuHandler(this, newEntry, dbHelper)

    lazy val intPartMoneyField = MoneyField(findViewById(R.id.int_part_money_field).asInstanceOf[TextView], findViewById(R.id.int_part_money_field_name).asInstanceOf[TextView])
    lazy val realPartMoneyField = MoneyField(findViewById(R.id.real_part_money_field).asInstanceOf[TextView], findViewById(R.id.real_part_money_field_name).asInstanceOf[TextView])

    private var activeMoneyField: MoneyField = null

    override def onCreate(savedInstanceState: Bundle)
    {
        super.onCreate(savedInstanceState)
//        deleteDatabase(getText(R.string.db_name).toString)
        setContentView(R.layout.main)
        processWallets()
        setListeners()
        setRegisters()
        setActiveMoneyField(intPartMoneyField)
    }

    override def onResume()
    {
        super.onResume()
        if(!newEntry.isNew)
            reinitEntry()
        setChangeEntryTypeButtonDefaultStyle()
        setDefaultWallet()
        setDefaultPrice()
    }

    private def reinitEntry()
    {
        val defaultEntry = new BaseEntry
        newEntry.setPrice(defaultEntry.getPrice)
        newEntry.setComment(defaultEntry.getComment)
        newEntry.setDate(defaultEntry.getDate)
        newEntry.setEntryType(defaultEntry.getEntryType)
        newEntry.setWallet(defaultEntry.getWallet)
    }

    override def onCreateContextMenu(menu: ContextMenu, v: View, menuInfo: ContextMenuInfo)
    {
        contextMenuHandler.onCreateContextMenu(menu, v, menuInfo)
    }

    def setActiveMoneyField(newActiveMoneyField: MoneyField)
    {
        this.activeMoneyField = newActiveMoneyField
    }

    def getActiveMoneyField = activeMoneyField

    private def setListeners()
    {
        val enterPriceListener = new EnterPriceOnClickListener(intPartMoneyField, realPartMoneyField, this)
        val changeEntryTypeListener = new ChangeEntryTypeListener(newEntry, this)
        setOnClickListener(R.id.button_0, enterPriceListener)
        setOnClickListener(R.id.button_1, enterPriceListener)
        setOnClickListener(R.id.button_2, enterPriceListener)
        setOnClickListener(R.id.button_3, enterPriceListener)
        setOnClickListener(R.id.button_4, enterPriceListener)
        setOnClickListener(R.id.button_5, enterPriceListener)
        setOnClickListener(R.id.button_6, enterPriceListener)
        setOnClickListener(R.id.button_7, enterPriceListener)
        setOnClickListener(R.id.button_8, enterPriceListener)
        setOnClickListener(R.id.button_9, enterPriceListener)
        setOnClickListener(R.id.button_comma, enterPriceListener)
        setOnClickListener(R.id.button_clear, enterPriceListener)
        setOnClickListener(R.id.int_part_money_field, enterPriceListener)
        setOnClickListener(R.id.int_part_money_field_name, enterPriceListener)
        setOnClickListener(R.id.real_part_money_field, enterPriceListener)
        setOnClickListener(R.id.real_part_money_field_name, enterPriceListener)
        setOnClickListener(R.id.change_price_type, changeEntryTypeListener)
        setOnClickListener(R.id.save_button, new MainScreenSaveButtonListener(newEntry, this))
        setOnClickListener(R.id.cancel_button, new MainScreenCancelButtonListener(newEntry, this))
        setOnClickListener(R.id.hide_keyboard_button, new HideKeyboardButtonListener(this))
        setOnClickListener(R.id.change_wallet, new ChangeWalletButtonListener(this))
    }

    private def setRegisters()
    {
        registerForContextMenu(findViewById(R.id.change_wallet))
    }

    private def setOnClickListener(viewId: Int, listener: OnClickListener)
    {
        findViewById(viewId).setOnClickListener(listener)
    }

    private def setChangeEntryTypeButtonDefaultStyle()
    {
        val changePriceTypeButton = findViewById(R.id.change_price_type).asInstanceOf[Button]
        setChangeEntryTypeButtonDefaultStyle(newEntry, changePriceTypeButton, getResources)
    }


    override def onContextItemSelected(item: MenuItem): Boolean =
        contextMenuHandler.onContextItemSelected(item)

    private def setDefaultWallet()
    {
        val v = findViewById(R.id.change_wallet)
        val walletStorage = WalletStorage.instance
        val dbHelper = new DBHelper(this)
        val wallets = walletStorage.getAllWallets(dbHelper)
        if(wallets.isEmpty)
        {
            val newWallet = getNewWallet
            v.asInstanceOf[Button].setText(newWallet.getName)
            newEntry.setWallet(newWallet)
        }
        else
        {
            v.asInstanceOf[Button].setText(wallets.head.getName)
            newEntry.setWallet(wallets.head)
        }
    }

    private def setDefaultPrice()
    {
        val intPricePartView = findViewById(R.id.int_part_money_field).asInstanceOf[TextView]
        val realPricePartView = findViewById(R.id.real_part_money_field).asInstanceOf[TextView]

        intPricePartView.setText(newEntry.getIntPricePart.toString)
        realPricePartView.setText(newEntry.getRealPricePart.toString)

        ActivePriceFieldSetter.setActiveIntField(intPartMoneyField, realPartMoneyField, this)
    }

    private def processWallets()
    {
        if(!isWalletsExists())
            getNewWallet
    }

    private def getNewWallet =
    {
        val newWallet = new Wallet()
        newWallet.setName(getText(R.string.default_wallet_name).toString)
        newWallet.doSaveOrUpdate(dbHelper)
        newWallet
    }

    private def isWalletsExists() =
    {
        val walletsCount = WalletStorage.instance.getCount(dbHelper)
        walletsCount > 0
    }
}