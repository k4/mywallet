package com.example.myWallet.activities

import com.example.myWallet.{DBHelper, R}
import android.view.{View, ViewGroup, LayoutInflater}
import android.os.Bundle
import android.app.{FragmentTransaction, ActionBar}
import android.app.ActionBar.{Tab, TabListener}
import android.support.v4.app.{Fragment, FragmentManager, FragmentStatePagerAdapter, FragmentActivity}
import android.support.v4.view.ViewPager
import java.util.Random
import android.graphics.Color
import android.widget.TextView
import com.example.myWallet.storages.WalletStorage
import com.example.myWallet.bo.Wallet
import com.example.myWallet.utils.StringUtils

/**
 * Created by Viktor on 15.04.2014.
 */

class Wallets extends FragmentActivity
{
    lazy val walletsPagerAdapter:  WalletsPagerAdapter = new WalletsPagerAdapter(getSupportFragmentManager, wallets)
    lazy val mViewPager: ViewPager = findViewById(R.id.wallets_pager).asInstanceOf[ViewPager]
    lazy val dbHelper = new DBHelper(this)
    lazy val wallets =
    {
        val walletStorage = WalletStorage.instance
        walletStorage.getAllWallets(dbHelper)
    }

    override def onCreate(savedInstanceState: Bundle)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.wallets)

        mViewPager.setAdapter(walletsPagerAdapter)

        val actionBar = getActionBar
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS)

        mViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener()
        {
            override def onPageSelected(position: Int)
            {
                getActionBar.setSelectedNavigationItem(position)
            }
        })

        setWalletTabs(actionBar, new WalletsTabListener(mViewPager))
    }

    private def setWalletTabs(actionBar: ActionBar, tabListener: TabListener)
    {
        wallets.foreach(wallet =>
        {
            actionBar.addTab(actionBar.newTab()
              .setText(wallet.getName)
              .setTabListener(tabListener))
        })
    }
}

class WalletsPagerAdapter(fm: FragmentManager, wallets: List[Wallet]) extends FragmentStatePagerAdapter(fm)
{
    override def getItem(position: Int): Fragment =
        WalletPageFragment.newInstance(position + 1, wallets(position))

    override def getCount: Int = wallets.size

    override def getPageTitle(position: Int): CharSequence = wallets(position).getName
}

class WalletsTabListener(mViewPager: ViewPager) extends TabListener
{
    override def onTabSelected(tab: Tab, ft: FragmentTransaction)
    {
        mViewPager.setCurrentItem(tab.getPosition)
    }

    override def onTabUnselected(tab: Tab, ft: FragmentTransaction)
    {

    }

    override def onTabReselected(tab: Tab, ft: FragmentTransaction)
    {

    }
}