package com.example.myWallet.activities

import com.example.myWallet.bo.{BaseEntry, EntryType, Wallet}
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.{View, ViewGroup, LayoutInflater}
import com.example.myWallet.{DBHelper, R}
import android.widget.{LinearLayout, ArrayAdapter, ListView, TextView}
import com.example.myWallet.utils.{DateUtils, StringUtils}
import com.example.myWallet.storages.BaseEntryStorage
import android.content.Context
import java.util.Date

/**
 * Created by Viktor on 11.05.2014.
 */

object WalletPageFragment
{
    val ARGUMENT_PAGE_NUMBER = "arg_page_number"

    def newInstance(page: Int, wallet: Wallet) =
    {
        val pageFragment = new WalletPageFragment(wallet)
        val arguments = new Bundle()
        arguments.putInt(ARGUMENT_PAGE_NUMBER, page)
        pageFragment.setArguments(arguments)
        pageFragment
    }
}

import WalletPageFragment._
class WalletPageFragment(wallet: Wallet) extends Fragment
{
    lazy val pageNumber = getArguments.getInt(ARGUMENT_PAGE_NUMBER)

    override def onCreateView(inflater: LayoutInflater, container: ViewGroup, savedInstanceState: Bundle): View =
    {
        val rootView = inflater.inflate(R.layout.wallet, container, false)

        setPriceFields(rootView)
        setEntriesList(rootView)

        rootView
    }

    private def setPriceFields(rootView: View)
    {
        val intPricePartView = rootView.findViewById(R.id.int_part_money_field).asInstanceOf[TextView]
        val realPricePartView = rootView.findViewById(R.id.real_part_money_field).asInstanceOf[TextView]

        intPricePartView.setText(StringUtils.formatToMoneyString(wallet.getIntPricePart.toString))
        realPricePartView.setText(Math.abs(wallet.getRealPricePart).toString)

        if(wallet.getPrice < 0)
        {
            val minusPricePartView = rootView.findViewById(R.id.minus_money_field).asInstanceOf[TextView]
            minusPricePartView.setText("-")
        }
    }

    private def setEntriesList(rootView: View)
    {
        val dbHelper = new DBHelper(rootView.getContext)
        val baseEntryStorage = BaseEntryStorage.instance

        val entries = baseEntryStorage.getEntriesForWallet(wallet.getId, 10, dbHelper)
        val entriesList = rootView.findViewById(R.id.entries_list).asInstanceOf[ListView]
        val entriesAdapter = new EntriesListAdapter(rootView.getContext, R.layout.entry_list_item, entries.toArray)

        entriesList.setAdapter(entriesAdapter)
    }
}

class EntriesListAdapter(context: Context, resource: Int, entries: Array[BaseEntry])
  extends ArrayAdapter[BaseEntry](context, resource, entries)
{
    override def getView(position: Int, convertView: View, parent: ViewGroup): View =
    {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE).asInstanceOf[LayoutInflater]
        val rootView = inflater.inflate(resource, parent, false)

        val entry = entries(position)
        setPriceValue(rootView, entry)
        setDate(rootView, entry)
        setBackground(rootView, position)

        rootView
    }

    private def setPriceValue(rootView: View, entry: BaseEntry)
    {
        val minusView = rootView.findViewById(R.id.minus_money_field).asInstanceOf[TextView]
        val intPricePartView = rootView.findViewById(R.id.int_part_money_field).asInstanceOf[TextView]
        val intPricePartNameView = rootView.findViewById(R.id.int_part_money_field_name).asInstanceOf[TextView]
        val realPricePartView = rootView.findViewById(R.id.real_part_money_field).asInstanceOf[TextView]
        val realPricePartNameView = rootView.findViewById(R.id.real_part_money_field_name).asInstanceOf[TextView]

        if(entry.getEntryType == EntryType.Out)
            minusView.setText("- ")
        else
            minusView.setText("+ ")

        intPricePartView.setText(StringUtils.formatToMoneyString(entry.getIntPricePart.toString))
        realPricePartView.setText(entry.getRealPricePart.toString)


        val colorId = entry.getEntryType match
        {
            case EntryType.In => R.color.button_type_color_in
            case EntryType.Out => R.color.button_type_color_out
        }

        val color = rootView.getResources.getColor(colorId)

        minusView.setTextColor(color)
        intPricePartView.setTextColor(color)
        intPricePartNameView.setTextColor(color)
        realPricePartView.setTextColor(color)
        realPricePartNameView.setTextColor(color)
    }

    private def setDate(rootView: View, entry: BaseEntry)
    {
        val itemDateView = rootView.findViewById(R.id.item_date).asInstanceOf[TextView]
        itemDateView.setText(DateUtils.getShortDateString(entry.getDate))
    }

    private def setBackground(rootView: View, position: Int)
    {
        if(position % 2 == 0)
        {
            val itemView = rootView.findViewById(R.id.item_view).asInstanceOf[LinearLayout]
            val color = rootView.getResources.getColor(R.color.item_background)
            itemView.setBackgroundColor(color)
        }
    }
}
