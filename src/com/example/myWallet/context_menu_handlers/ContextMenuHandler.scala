package com.example.myWallet.context_menu_handlers

import android.view.{MenuItem, View, ContextMenu}
import android.view.ContextMenu.ContextMenuInfo

/**
 * Created by Viktor on 07.05.2014.
 */
trait ContextMenuHandler
{
    def onCreateContextMenu(menu: ContextMenu, v: View, menuInfo: ContextMenuInfo)
    {}

    def onContextItemSelected(item: MenuItem): Boolean = true
}
