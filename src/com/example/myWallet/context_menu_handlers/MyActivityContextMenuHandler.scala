package com.example.myWallet.context_menu_handlers

import android.view.{MenuItem, View, ContextMenu}
import android.view.ContextMenu.ContextMenuInfo
import com.example.myWallet.{DBHelper, R}
import com.example.myWallet.storages.WalletStorage
import com.example.myWallet.bo.BaseEntry
import android.database.sqlite.SQLiteOpenHelper
import android.app.Activity
import android.widget.Button

/**
 * Created by Viktor on 07.05.2014.
 */
class MyActivityContextMenuHandler(activity: Activity, entry: BaseEntry, dbHelper: SQLiteOpenHelper) extends ContextMenuHandler
{
    override def onCreateContextMenu(menu: ContextMenu, v: View, menuInfo: ContextMenuInfo)
    {
        v.getId match
        {
            case R.id.change_wallet =>
            {
                val walletStorage = WalletStorage.instance
                val allWallets = walletStorage.getAllWallets(new DBHelper(activity))
                allWallets.foreach(wallet => menu.add(0, wallet.getId.toInt, 0, wallet.getName))
            }
            case _ =>
        }
    }

    override def onContextItemSelected(item: MenuItem): Boolean =
    {
        changeWallet(item)
        super.onContextItemSelected(item)
    }

    private def changeWallet(item: MenuItem)
    {
        item.getItemId match
        {
            case -1 =>
            case _ =>
            {
                val walletStorage = WalletStorage.instance
                val wallet = walletStorage.getWalletById(item.getItemId, dbHelper)
                if(wallet != null)
                {
                    entry.setWallet(wallet)
                    val changeWalletButton = activity.findViewById(R.id.change_wallet).asInstanceOf[Button]
                    changeWalletButton.setText(wallet.getName)
                }
            }
        }
    }
}
