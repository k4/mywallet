package com.example.myWallet.listeners

import com.example.myWallet.bo.{EntryType, BaseEntry}
import android.widget.Button
import android.content.res.Resources
import com.example.myWallet.R

/**
 * Created by Viktor on 26.04.2014.
 */
trait ChangeEntryTypeButtonDefaultStyle
{
    def setChangeEntryTypeButtonDefaultStyle(entry: BaseEntry, button: Button, resources: Resources)
    {
        entry.getEntryType match
        {
            case EntryType.In =>
            {
                button.setBackgroundResource(R.drawable.my_change_price_button_in)
                button.setText(R.string.income_text)
                button.setTextColor(resources.getColor(R.color.change_price_type_color_in))
            }
            case EntryType.Out =>
            {
                button.setBackgroundResource(R.drawable.my_change_price_button_out)
                button.setText(R.string.outcome_text)
                button.setTextColor(resources.getColor(R.color.change_price_type_color_out))
            }
        }
    }
}
