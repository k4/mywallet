package com.example.myWallet.listeners

import android.view.View.OnClickListener
import android.app.Activity
import android.view.View
import com.example.myWallet.bo.{EntryType, BaseEntry}
import com.example.myWallet.R
import EntryType.EntryType

class ChangeEntryTypeListener(entry: BaseEntry, activity: Activity) extends OnClickListener
    with ChangeEntryType
{
    def onClick(changeTypeButton: View)
    {
        entry.getEntryType match
        {
            case EntryType.In =>
            {
                val color = activity.getResources.getColor(R.color.change_price_type_color_out)
                changeEntryType(changeTypeButton, EntryType.Out, R.drawable.my_change_price_button_out, R.string.outcome_text, color)
            }
            case EntryType.Out =>
            {
                val color = activity.getResources.getColor(R.color.change_price_type_color_in)
                changeEntryType(changeTypeButton, EntryType.In, R.drawable.my_change_price_button_in, R.string.income_text, color)
            }
        }
    }

    private def changeEntryType(button: View, newType: EntryType, newStyleId: Int, textId: Int, textColor: Int)
    {
        changeTypeForEntry(entry, button, newType, newStyleId, textId, textColor)
    }
}
