package com.example.myWallet.listeners

import android.view.View.OnClickListener
import android.view.View
import com.example.myWallet.bo.{EntryType, BaseEntry}
import android.app.Activity
import com.example.myWallet.R
import android.widget.{Button, TextView}

/**
 * User: vkrasnov
 * Date: 09.04.14
 * Time: 21:20
 */
class MainScreenCancelButtonListener(entry: BaseEntry, activity: Activity) extends OnClickListener
    with ChangeEntryTypeButtonDefaultStyle
{
    def onClick(v: View)
    {
        clearEntry()
        clearActivity()
    }

    private def clearActivity()
    {
        activity.findViewById(R.id.real_part_money_field).asInstanceOf[TextView].setText(entry.getIntPricePart.toString)
        activity.findViewById(R.id.int_part_money_field).asInstanceOf[TextView].setText(entry.getRealPricePart.toString)
        setChangeEntryTypeButtonDefaultStyle(entry, activity.findViewById(R.id.change_price_type).asInstanceOf[Button], activity.getResources)
    }

    private def clearEntry()
    {
        entry.setEntryType(EntryType.Out)
        entry.setIntPricePart(0)
        entry.setRealPricePart(0)
    }
}
