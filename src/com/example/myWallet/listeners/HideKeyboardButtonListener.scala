package com.example.myWallet.listeners

import android.view.View.OnClickListener
import android.view.View
import android.app.Activity
import com.example.myWallet.R
import android.widget.Button

/**
 * User: vkrasnov
 * Date: 10.04.14
 * Time: 21:08
 */
class HideKeyboardButtonListener(activity: Activity) extends OnClickListener
{
    def onClick(v: View)
    {
        val numberKeyboardView = activity.findViewById(R.id.number_keyboard)
        if(numberKeyboardView.getVisibility == View.VISIBLE)
        {
            numberKeyboardView.setVisibility(View.GONE)
            v.asInstanceOf[Button].setText(R.string.show_keyboard)
        }
        else
        {
            numberKeyboardView.setVisibility(View.VISIBLE)
            v.asInstanceOf[Button].setText(R.string.hide_keyboard)
        }
    }
}
