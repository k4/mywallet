package com.example.myWallet.listeners

import android.content.Context
import android.view.View.OnClickListener
import android.view.View
import android.app.Activity
import com.example.myWallet.R

/**
 * Created by Viktor on 08.05.2014.
 */
class ChangeWalletButtonListener(context: Context) extends OnClickListener
{
    override def onClick(v: View)
    {
        v.getId match
        {
            case R.id.change_wallet => context.asInstanceOf[Activity].openContextMenu(v)
            case _ =>
        }
    }
}
