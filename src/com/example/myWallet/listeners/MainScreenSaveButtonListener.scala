package com.example.myWallet.listeners

import android.view.View.OnClickListener
import android.view.View
import com.example.myWallet.bo.{EntryType, Wallet, BaseEntry}
import com.example.myWallet.utils.StringUtils
import com.example.myWallet.{DBHelper, R}
import android.app.Activity
import android.widget.TextView
import java.util.Date
import android.content.Intent
import com.example.myWallet.activities.Wallets
import com.example.myWallet.storages.WalletStorage

/**
 * User: vkrasnov
 * Date: 09.04.14
 * Time: 21:20
 */
class MainScreenSaveButtonListener(entry: BaseEntry, activity: Activity) extends OnClickListener
{
    lazy val dbHelper = new DBHelper(activity)

    def onClick(v: View)
    {
        setEntryValues()
        if(entry.getPrice != 0)
            saveEntry()
        val wallets_class = classOf[Wallets]
        val intent = new Intent(activity, wallets_class)
        activity.startActivity(intent)
    }

    private def setEntryValues()
    {
        val intValueField = activity.findViewById(R.id.int_part_money_field).asInstanceOf[TextView]
        val realValueField = activity.findViewById(R.id.real_part_money_field).asInstanceOf[TextView]
        entry.setId(-1)
        entry.setIntPricePart(StringUtils.getIntFromString(intValueField.getText.toString))
        entry.setRealPricePart(StringUtils.getIntFromString(realValueField.getText.toString))
        entry.setDate(new Date)
    }

    private def saveEntry()
    {
        val wallet = entry.getWallet
        if(wallet.getInited)
        {
            changeWalletPrice(wallet)
            wallet.doSaveOrUpdate(dbHelper)
        }
        else
        {
            val walletStorage = WalletStorage.instance
            val initedWallet = walletStorage.getWalletById(entry.getWallet.getId, dbHelper)
            entry.setWallet(initedWallet)
            changeWalletPrice(initedWallet)
            initedWallet.doSaveOrUpdate(dbHelper)
        }
        entry.doSaveOrUpdate(dbHelper)
    }

    private def changeWalletPrice(wallet: Wallet)
    {
        entry.getEntryType match
        {
            case EntryType.In => wallet.addPrice(entry)
            case EntryType.Out => wallet.removePrice(entry)
            case _ =>
        }
    }
}
