package com.example.myWallet.listeners

import android.view.View.OnClickListener
import android.view.View
import com.example.myWallet.{R}
import android.app.Activity
import com.example.myWallet.utils.StringUtils
import android.widget.TextView
import com.example.myWallet.activities.{MyActivity, MoneyField}

/**
 * User: vkrasnov
 * Date: 06.04.14
 * Time: 15:29
 */

object ActivePriceFieldSetter
{
    def setActiveIntField(intPartMoneyField: MoneyField, realPartMoneyField: MoneyField, activity: MyActivity)
    {
        intPartMoneyField.valueField.setTextAppearance(activity.getApplicationContext, R.style.MoneyField_Active)
        intPartMoneyField.nameField.setTextAppearance(activity.getApplicationContext, R.style.MoneyField_Active)
        realPartMoneyField.valueField.setTextAppearance(activity.getApplicationContext, R.style.MoneyField)
        realPartMoneyField.nameField.setTextAppearance(activity.getApplicationContext, R.style.MoneyField)
        activity.setActiveMoneyField(intPartMoneyField)
    }

    def setActiveRealField(intPartMoneyField: MoneyField, realPartMoneyField: MoneyField, activity: MyActivity)
    {
        intPartMoneyField.valueField.setTextAppearance(activity.getApplicationContext, R.style.MoneyField)
        intPartMoneyField.nameField.setTextAppearance(activity.getApplicationContext, R.style.MoneyField)
        realPartMoneyField.valueField.setTextAppearance(activity.getApplicationContext, R.style.MoneyField_Active)
        realPartMoneyField.nameField.setTextAppearance(activity.getApplicationContext, R.style.MoneyField_Active)
        activity.setActiveMoneyField(realPartMoneyField)
    }
}

class EnterPriceOnClickListener(intPartMoneyField: MoneyField, realPartMoneyField: MoneyField, activity: MyActivity) extends OnClickListener
{
    val resources = activity.getResources

    val maxEnterIntSum = resources.getInteger(R.integer.max_enter_price)

    def onClick(viewButton: View)
    {
        viewButton.getId match
        {
            case R.id.button_0 => pressNumberButton(0)
            case R.id.button_1 => pressNumberButton(1)
            case R.id.button_2 => pressNumberButton(2)
            case R.id.button_3 => pressNumberButton(3)
            case R.id.button_4 => pressNumberButton(4)
            case R.id.button_5 => pressNumberButton(5)
            case R.id.button_6 => pressNumberButton(6)
            case R.id.button_7 => pressNumberButton(7)
            case R.id.button_8 => pressNumberButton(8)
            case R.id.button_9 => pressNumberButton(9)
            case R.id.button_clear => pressClearButton()
            case R.id.button_comma => pressCommaButton()
            case R.id.int_part_money_field => setActiveIntField()
            case R.id.int_part_money_field_name => setActiveIntField()
            case R.id.real_part_money_field => setActiveRealField()
            case R.id.real_part_money_field_name => setActiveRealField()
        }
    }

    private def setActiveIntField()
    {
        ActivePriceFieldSetter.setActiveIntField(intPartMoneyField, realPartMoneyField, activity)
        activity.setActiveMoneyField(intPartMoneyField)
    }

    private def setActiveRealField()
    {
        ActivePriceFieldSetter.setActiveRealField(intPartMoneyField, realPartMoneyField, activity)
        activity.setActiveMoneyField(realPartMoneyField)
    }

    private def pressCommaButton()
    {
        if(activity.getActiveMoneyField == intPartMoneyField)
            setActiveRealField()
        else
            setActiveIntField()
    }

    private def pressNumberButton(number: Int)
    {
        if(activity.getActiveMoneyField == intPartMoneyField)
            pressNumberButtonForIntPart(number)
        else
            pressNumberButtonForRealPart(number)
    }

    private def pressNumberButtonForIntPart(number: Int)
    {
        val intValueField = intPartMoneyField.valueField
        val value = StringUtils.getIntFromString(intValueField.getText.toString)
        val newValue = value*10 + number
        if(value < (maxEnterIntSum - number)/10.0)
            setIntValueFieldValue(intValueField, newValue.toString)
    }

    private def pressNumberButtonForRealPart(number: Int)
    {
        val realValueField = realPartMoneyField.valueField
        val value = StringUtils.getIntFromString(realValueField.getText.toString)
        val newValue = value*10 + number
        if(newValue < 100)
            realValueField.setText(newValue.toString)
    }

    private def pressClearButton()
    {
        if(activity.getActiveMoneyField == intPartMoneyField)
            pressClearButtonForIntPart()
        else
            pressClearButtonForRealPart()
    }

    private def pressClearButtonForIntPart()
    {
        val intValueField = intPartMoneyField.valueField
        val value = StringUtils.getIntFromString(intValueField.getText.toString)
        if(value != 0)
            setIntValueFieldValue(intValueField, (value/10).toString)
        else
            pressClearButtonForRealPart()
    }

    private def pressClearButtonForRealPart()
    {
        val realValueField = realPartMoneyField.valueField
        val value = StringUtils.getIntFromString(realValueField.getText.toString)
        if(value != 0)
            realValueField.setText((value/10).toString)
        else
        {
            val intValueField = intPartMoneyField.valueField
            val intValue = StringUtils.getIntFromString(intValueField.getText.toString)
            if(intValue != 0)
                setIntValueFieldValue(intValueField, (intValue/10).toString)
        }
    }

    private def setIntValueFieldValue(intValueField: TextView, value: String)
    {
        intValueField.setText(StringUtils.formatToMoneyString(value))
    }
}
