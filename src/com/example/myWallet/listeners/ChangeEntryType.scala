package com.example.myWallet.listeners

import com.example.myWallet.bo.{EntryType, BaseEntry}
import android.view.View
import EntryType.EntryType
import android.widget.Button

/**
 * User: vkrasnov
 * Date: 07.04.14
 * Time: 21:09
 */

trait ChangeEntryType
{
    def changeTypeForEntry(entry: BaseEntry, button: View, newType: EntryType, newStyleId: Int, textId: Int, textColor: Int)
    {
        entry.setEntryType(newType)
        button.setBackgroundResource(newStyleId)
        button.asInstanceOf[Button].setText(textId)
        button.asInstanceOf[Button].setTextColor(textColor)
    }
}
