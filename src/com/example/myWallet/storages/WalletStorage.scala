package com.example.myWallet.storages

import android.database.sqlite.SQLiteOpenHelper
import com.example.myWallet.bo.{BoSave, Wallet}
import android.database.Cursor

/**
 * Created by Viktor on 27.04.2014.
 */

object WalletStorage
{
    val instance = new WalletStorage

    val TABLE_NAME = "WALLET"
    val ID_COLUMN = "ID"
    val NAME_COLUMN = "NAME"
    val PRICE_COLUMN = "PRICE"
}

import WalletStorage._

class WalletStorage extends DataBaseStorage[Wallet]
{
    override def getTableName: String = TABLE_NAME

    override def getPrimaryKey: String = ID_COLUMN

    override def getCreateTableSQL(): String =
    {
        s"""
           |create table $getTableName (
           |    $getPrimaryKey integer primary key autoincrement,
           |    $NAME_COLUMN text,
           |    $PRICE_COLUMN long
           |);
         """.stripMargin
    }
    
    override def getNewBo: Wallet = new Wallet()

    def getAllWallets(dbHelper: SQLiteOpenHelper): List[Wallet] =
        getListFromQuery(dbHelper, s"select * from $TABLE_NAME")
    
    def getWalletById(id: Long, dbHelper: SQLiteOpenHelper): Wallet =
    {
        val wallets = getListFromQuery(dbHelper, s"select * from $TABLE_NAME where id=$id")
        if(wallets.nonEmpty)
            wallets.head
        else
            null
    }
}
