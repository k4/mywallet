package com.example.myWallet.storages

/**
 * Created by Viktor on 27.04.2014.
 */
object BaseEntryStorage
{
    val instance = new BaseEntryStorage

    val TABLE_NAME = "BASE_ENTRY"
    val ID_COLUMN = "ID"
    val ENTRY_TYPE_COLUMN = "ENTRY_TYPE"
    val COMMENT_COLUMN = "COMMENT"
    val PRICE_COLUMN = "PRICE"
    val DATE_COLUMN = "DATE"
    val WALLET_COLUMN = "WALLET"
}

import BaseEntryStorage._
import com.example.myWallet.bo.BaseEntry
import android.database.sqlite.SQLiteOpenHelper

class BaseEntryStorage extends DataBaseStorage[BaseEntry]
{
    private val walletStorage = WalletStorage.instance

    override def getTableName = TABLE_NAME

    override def getPrimaryKey = ID_COLUMN

    override def getCreateTableSQL(): String =
    {
        s"""
           |create table $getTableName (
           |    $getPrimaryKey integer primary key autoincrement,
           |    $ENTRY_TYPE_COLUMN integer,
           |    $COMMENT_COLUMN text,
           |    $PRICE_COLUMN long,
           |    $DATE_COLUMN numeric,
           |    $WALLET_COLUMN integer,
           |    foreign key($WALLET_COLUMN)references $getWalletTableName($getWalletPrimaryKey)
           |);
         """.stripMargin
    }

    private def getWalletTableName = walletStorage.getTableName

    private def getWalletPrimaryKey = walletStorage.getPrimaryKey

    override def getNewBo: BaseEntry = new BaseEntry()

    def getEntriesForWallet(walletId: Long, dbHelper: SQLiteOpenHelper): List[BaseEntry] =
        getListFromQuery(dbHelper, s"select * from $TABLE_NAME where $WALLET_COLUMN = ? order by $DATE_COLUMN desc", Array(walletId.toString)).reverse

    def getEntriesForWallet(walletId: Long, entriesCount: Int, dbHelper: SQLiteOpenHelper): List[BaseEntry] =
        getListFromQuery(dbHelper, s"select * from $TABLE_NAME where $WALLET_COLUMN = ? order by $DATE_COLUMN desc limit $entriesCount", Array(walletId.toString)).reverse
}
