package com.example.myWallet.storages

import com.example.myWallet.bo.{Wallet, BoSave}
import android.database.Cursor
import android.database.sqlite.SQLiteOpenHelper

/**
 * Created by Viktor on 27.04.2014.
 */
trait DataBaseStorage[T <: BoSave]
{
    def getTableName: String

    def getCreateTableSQL: String

    def getPrimaryKey: String

    def getNewBo: T

    private def getListFromCursor(cursor: Cursor): List[T] =
        getListFromCursor(cursor, List())

    private def getListFromCursor(cursor: Cursor, bos: List[T]): List[T] =
    {
        val bo = getNewBo
        bo.loadFromCursor(cursor)
        if(cursor.moveToNext())
            getListFromCursor(cursor, bo :: bos)
        else
            bo :: bos
    }

    protected def getListFromQuery(dbHelper: SQLiteOpenHelper, sql: String): List[T] =
        getListFromQuery(dbHelper, sql, null)

    protected def getListFromQuery(dbHelper: SQLiteOpenHelper, sql: String, selectionArgs: Array[String]): List[T] =
    {
        val db = dbHelper.getWritableDatabase
        val requestCursor = db.rawQuery(sql, selectionArgs)
        if(requestCursor.moveToFirst())
        {
            val bos = getListFromCursor(requestCursor, List())
            requestCursor.close()
            bos
        }
        else
        {
            requestCursor.close()
            List()
        }
    }

    def getCount(dbHelper: SQLiteOpenHelper): Int =
        getCount(dbHelper, "", null)

    def getCount(dbHelper: SQLiteOpenHelper, sqlCondition: String): Int =
        getCount(dbHelper, sqlCondition, null)

    def getCount(dbHelper: SQLiteOpenHelper, sqlCondition: String, selectionArgs: Array[String]): Int =
    {
        val db = dbHelper.getWritableDatabase
        val bosCountCursor =
        {
            if(sqlCondition == null || sqlCondition.trim.isEmpty)
                db.rawQuery(s"select count(*) from $getTableName", selectionArgs)
            else
                db.rawQuery(s"select count(*) from $getTableName where " + sqlCondition, selectionArgs)
        }
        bosCountCursor.moveToFirst()
        val count = bosCountCursor.getInt(0)
        bosCountCursor.close()
        count
    }
}
