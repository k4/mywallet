package com.example.myWallet.storages

import com.example.myWallet.bo.{BaseEntry, Wallet}

/**
 * Created by Viktor on 28.04.2014.
*/

object StorageManager
{
    lazy val instance = new StorageManager
}

class StorageManager
{
    private val classStorages: Map[Class[_], _ <: DataBaseStorage[_]] = Map(
        classOf[Wallet] -> WalletStorage.instance,
        classOf[BaseEntry] -> BaseEntryStorage.instance
      )

    def getStorage(clazz: Class[_]) =
    {
        (classStorages get clazz) match
        {
            case Some(storage) => storage
            case None => null
        }
    }
}
