package com.example.myWallet.bo

import android.database.sqlite.SQLiteOpenHelper
import com.example.myWallet.storages.{DataBaseStorage, StorageManager}
import android.content.ContentValues
import com.example.myWallet.bo.EntryType.EntryType
import android.database.Cursor
import java.util.Date

/**
 * Created by Viktor on 26.04.2014.
 */

case class ParamProcessor(param: String, getter: Any)

trait BoSave
{
    private var id: Long = -1
    private var inited: Boolean = false

    def setId(newId: Long)
    {
        this.id = newId
    }

    def getId = id

    def setInited(newInited: Boolean)
    {
        this.inited = newInited
    }

    def getInited = inited

    def getParamProcessors: List[ParamProcessor]

    def doSaveOrUpdate(dbHelper: SQLiteOpenHelper) = synchronized
    {
        try
        {
            val storageManager = StorageManager.instance
            val db = dbHelper.getWritableDatabase
            val cv = new ContentValues()
            for (pp <- getParamProcessors)
                putValue(cv, pp.param, pp.getter)
            val storage: DataBaseStorage[_] = storageManager.getStorage(this.getClass)
            if(getId == -1)
            {
                val rowId = db.insert(storage.getTableName, null, cv)
                setId(rowId)
            }
            else
                db.update(storage.getTableName, cv, "id = ?", Array[String](getId.toString))
            dbHelper.close()
        }
        catch
        {
            case _: Throwable => //TODO Обработку ошибок
        }
    }

    private def putValue(cv: ContentValues, paramName: String, value: Any)
    {
        value match
        {
            case i: Int => cv.put(paramName, i.asInstanceOf[java.lang.Integer])
            case l: Long => cv.put(paramName, l.asInstanceOf[java.lang.Long])
            case s: String => cv.put(paramName, s)
            case d: Double => cv.put(paramName, d)
            case b: BoSave => cv.put(paramName, b.getId.asInstanceOf[java.lang.Long])
            case d: Date => cv.put(paramName, d.getTime.asInstanceOf[java.lang.Long])
            case e: EntryType => cv.put(paramName, e.id.asInstanceOf[java.lang.Integer])
            case _ =>
        }
    }

    def loadFromCursor(cursor: Cursor): BoSave =
    {
        this
    }
}
