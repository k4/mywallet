package com.example.myWallet.bo

/**
 * Created by Viktor on 10.05.2014.
 */

trait PriceContainer
{
    def getPrice: Long = 0

    def getIntPricePart: Long =
        Math.abs(getPrice) / 100

    def getRealPricePart: Long =
        Math.abs(getPrice) % 100

    def setPrice(newPrice: Long): Unit

    def setIntPricePart(intPricePart: Long): Unit =
    {
        setPrice(intPricePart* 100 + getRealPricePart)
    }

    def setRealPricePart(realPricePart: Int): Unit =
    {
        setPrice(getIntPricePart* 100 + realPricePart)
    }

    def addPrice(anotherPriceContainer: PriceContainer)
    {
        setPrice(this.getPrice + anotherPriceContainer.getPrice)
    }

    def removePrice(anotherPriceContainer: PriceContainer)
    {
        setPrice(this.getPrice - anotherPriceContainer.getPrice)
    }
}
