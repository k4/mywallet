package com.example.myWallet.bo

import com.example.myWallet.storages.WalletStorage
import android.database.Cursor

/**
 * Created by Viktor on 26.04.2014.
 */
class Wallet extends BoSave with PriceContainer
{
    private var name: String = ""
    private var price: Long = 0

    override def getParamProcessors: List[ParamProcessor] =
    {
        import WalletStorage._
        List(
            ParamProcessor(NAME_COLUMN, getName),
            ParamProcessor(PRICE_COLUMN, getPrice)
        )
    }
    
    def setName(newName: String)
    {
        this.name = newName
    }

    def getName = name

    override def getPrice = price

    override def setPrice(newPrice: Long)
    {
        this.price = newPrice
    }

    override def loadFromCursor(cursor: Cursor): BoSave =
    {
        import WalletStorage._
        val nameCI = cursor.getColumnIndex(NAME_COLUMN)
        val priceCI = cursor.getColumnIndex(PRICE_COLUMN)
        val idCI = cursor.getColumnIndex(ID_COLUMN)

        setInited(true)
        setId(cursor.getLong(idCI))
        setName(cursor.getString(nameCI))
        setPrice(cursor.getLong(priceCI))

        this
    }
}
