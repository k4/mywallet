package com.example.myWallet.bo

import EntryType.EntryType
import java.util.Date
import android.database.Cursor
import com.example.myWallet.storages.BaseEntryStorage

/**
 * User: vkrasnov
 * Date: 08.04.14
 * Time: 20:18
 */
class BaseEntry extends BoSave with PriceContainer
{
    private var entryType: EntryType = EntryType.Out
    private var comment: String = ""
    private var price: Long = 0
    private var date: Date = null
    private var wallet: Wallet = null


    override def getParamProcessors: List[ParamProcessor] =
    {
        import com.example.myWallet.storages.BaseEntryStorage._
        List(
            ParamProcessor(ENTRY_TYPE_COLUMN, getEntryType),
            ParamProcessor(COMMENT_COLUMN, getComment),
            ParamProcessor(PRICE_COLUMN, getPrice),
            ParamProcessor(DATE_COLUMN, getDate),
            ParamProcessor(WALLET_COLUMN, getWallet)
        )
    }

    def isNew = getId < 0

    def setEntryType(newEntryType: EntryType)
    {
        this.entryType = newEntryType
    }
    
    def getEntryType = entryType
    
    def setComment(newComment: String)
    {
        this.comment = newComment
    }

    def getComment = comment

    override def getPrice = price

    override def setPrice(newPrice: Long)
    {
        this.price = newPrice
    }

    def setDate(newDate: Date)
    {
        this.date = newDate
    }

    def getDate = date
    
    def setWallet(newWallet: Wallet)
    {
        this.wallet = newWallet    
    }
    
    def getWallet = wallet

    override def loadFromCursor(cursor: Cursor): BoSave =
    {
        import BaseEntryStorage._

        val idCI = cursor.getColumnIndex(ID_COLUMN)
        val entryTypeCI = cursor.getColumnIndex(ENTRY_TYPE_COLUMN)
        val commentCI = cursor.getColumnIndex(COMMENT_COLUMN)
        val priceCI = cursor.getColumnIndex(PRICE_COLUMN)
        val dateCI = cursor.getColumnIndex(DATE_COLUMN)
        val walletCI = cursor.getColumnIndex(WALLET_COLUMN)


        setInited(true)
        setId(cursor.getLong(idCI))
        setEntryType(EntryType.apply(cursor.getInt(entryTypeCI)))
        setComment(cursor.getString(commentCI))
        setPrice(cursor.getLong(priceCI))
        setDate(new Date(cursor.getLong(dateCI)))

        val wallet = new Wallet()
        wallet.setId(cursor.getLong(walletCI))
        setWallet(wallet)

        this
    }
}
