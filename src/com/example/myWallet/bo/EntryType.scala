package com.example.myWallet.bo

/**
 * User: vkrasnov
 * Date: 08.04.14
 * Time: 20:18
 */
object EntryType extends Enumeration
{
    type EntryType = Value
    val In, Out = Value
}
