package com.example.myWallet.utils

/**
 * User: vkrasnov
 * Date: 06.04.14
 * Time: 16:28
 */
object StringUtils
{
    def getIntFromString(str: String) =
    {
        val newStr = str.filter(_ != ' ')
        if(newStr.nonEmpty)
        {
            try
                newStr.toInt
            catch
            {
                case e: Exception => 0
            }
        }
        else 0
    }

    def formatToMoneyString(str: String) =
    {
        str.reverse.zipWithIndex.foldLeft("")((res, ci) =>
        {
            val (c, i) = (ci._1, ci._2)
            (if((i + 1)%3 == 0) " " + c else c) + res
        }).trim
    }
}
