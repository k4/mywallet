package com.example.myWallet.utils

import java.util.{GregorianCalendar, Calendar, Date}
import java.text.SimpleDateFormat

/**
 * Created by Viktor on 11.05.2014.
 */

case class Month(name: String, genName: String, shortName: String)
case class Day(name: String, genName: String, shortName: String)

object DateUtils
{
    val MONTHS = Array(
        Month("Январь", "Января", "Янв"),
        Month("Февраль", "Февраля", "Фев"),
        Month("Март", "Марта", "Мар"),
        Month("Апрель", "Апреля", "Апр"),
        Month("Май", "Мая", "Май"),
        Month("Июнь", "Июня", "Июн"),
        Month("Июль", "Июля", "Июл"),
        Month("Август", "Августа", "Авг"),
        Month("Сентябрь", "Сентября", "Сен"),
        Month("Октябрь", "Октября", "Окт"),
        Month("Ноябрь", "Ноября", "Ноя"),
        Month("Декабрь", "Декабря", "Дек")
    )

    val DAYS = Array(
        Day("Понедельник", "Понедельника", "Пон"),
        Day("Вторник", "Вторника", "Вт"),
        Day("Среда", "Среды", "Ср"),
        Day("Четверг", "Четверга", "Чт"),
        Day("Пятница", "Пятницы", "Птн"),
        Day("Суббота", "Субботы", "Суб"),
        Day("Воскресенье", "Воскресенья", "Вск")
    )

    def getShortDateString(date: Date): String =
    {
        val sdf = new SimpleDateFormat("dd.MM.yy HH:mm")
        sdf.format(date)
    }
}
