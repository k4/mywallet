package com.example.myWallet

import android.database.sqlite.{SQLiteDatabase, SQLiteOpenHelper}
import android.content.Context
import com.example.myWallet.storages.{BaseEntryStorage, WalletStorage, DataBaseStorage}

/**
 * Created by Viktor on 27.04.2014.
 */

class DBHelper(context: Context) extends SQLiteOpenHelper(context, context.getText(R.string.db_name).toString, null, 1)
{
    override def onCreate(db: SQLiteDatabase)
    {
        val storages = getStorages
        storages.foreach(s => db.execSQL(s.getCreateTableSQL))
    }

    override def onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int)
    {
//        val storages = getStorages
//        storages.foreach(s =>
//            {
//                val tableName = s.getTableName
//                db.execSQL(s"""drop table if exists $tableName""")
//            })
    }

    private def getStorages: List[DataBaseStorage[_]] =
    {
        List(
            WalletStorage.instance,
            BaseEntryStorage.instance
        )
    }
}
